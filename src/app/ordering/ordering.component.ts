import { Component, OnInit } from '@angular/core';
import { OrderingService } from '../ordering.service';

@Component({
  selector: 'app-ordering',
  templateUrl: './ordering.component.html',
  styleUrls: ['./ordering.component.css']
})
export class OrderingComponent implements OnInit {

  

  constructor(private os: OrderingService) { }

  ngOnInit(): void {
  }

}
