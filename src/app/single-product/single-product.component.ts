import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})
export class SingleProductComponent implements OnInit {
   
  categories?:Category;
  item?:Product;
  routeId?:string;
  arg?:string;
 


  constructor(private ps: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.arg = params['arg'];
    });
    // Fragment variable de l'URL, AVANT le ?
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
    });
    this.ps.getById(Number(this.routeId)).subscribe(data => this.item = data);

  }

}
