import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  
  products?:Product[];
  categories?:Category[];
  selectedCategory?:Category;
  

  constructor(private cs: CategoryService, private ps: ProductService) { }

  ngOnInit(): void {

    this.cs.getAll().subscribe(data => this.categories=data);
  }

  fetchProducts() {
    if(this.selectedCategory)
    this.ps.getProductsByCategoryId(this.selectedCategory.id!).subscribe(data=>this.products=data);
  }

}
