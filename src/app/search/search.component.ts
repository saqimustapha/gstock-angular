import { Component, OnInit } from '@angular/core';
import { Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  nameFilter?:string;
  products?:Product[] = [];
  displayResultList:boolean = false;

  constructor(private ps: ProductService) { 
    this.nameFilter = "";
  }

  ngOnInit(): void {
  }

  searchByName() {
    if (this.nameFilter?.length == 0) {
      alert("Merci de saisir une valeur !");
      document.getElementById('nameFilter')?.focus();
    } else {
      console.log(this.nameFilter)
      this.ps.search(this.nameFilter!).subscribe(
        data => this.products = data   
      );
      this.displayResultList = true;
    }
  }

}
