import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  
  categories:Category[] = [];
  products:Product = {
    price: 0,
    name: '',
    quantity:0,
    
  };

  constructor(private ps:ProductService, private router:Router, private cs: CategoryService) { }

  ngOnInit(): void {
    this.cs.getAll().subscribe(data => this.categories = data);
  }

  addProduct() {
   
    this.ps.add(this.products).subscribe(() => {
      this.router.navigate(['/']);
    })
  }

}
