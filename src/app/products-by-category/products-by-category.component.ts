import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../entities';
import { ProductService } from '../product.service';
import { switchMap } from 'rxjs';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-products-by-category',
  templateUrl: './products-by-category.component.html',
  styleUrls: ['./products-by-category.component.css']
})
export class ProductsByCategoryComponent implements OnInit {

  products?:Product[] = [];

  constructor(private ps: ProductService, private cs :CategoryService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.params.pipe(
      switchMap(params => this.ps.getProductsByCategoryId(params['id']))
    ).subscribe(data => this.products = data
    );

  }

}
