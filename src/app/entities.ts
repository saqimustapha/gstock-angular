
export interface Product {
  id?:number;
  name:string;
  price:number;
  quantity:number;
  category?:Category;
  
}

export interface Category {
  id?:number;
  designation:string;
  products: Product[];
}

export interface Ordering {
  id?:number,
  quantity:number,
  date:string,
  product:Product
}
export interface User {
  id?:number;
  email:string;
  password?:string;
  role?:string;
  
}



  
  
 
