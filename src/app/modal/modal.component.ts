import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ordering, Product } from '../entities';
import { ProductService } from '../product.service';
import { switchMap } from 'rxjs';
import { OrderingService } from '../ordering.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
 
  laClasse = 'Selected';
  product:Product = {
    price: 0,
    name: '',
    quantity:0
  };
  order:Ordering={
    quantity :0,
    date:'',
    product:this.product

  }

  saveProduct() {
    this.os.add(this.order).subscribe();
    //appeler la foction save de l ordering service en lui passant order
    console.log(this.order); 
  }
 
  modalOpen:boolean = false;

  constructor(private ps:ProductService, private router:Router, private route: ActivatedRoute, private os: OrderingService) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.ps.getById(params['id']))
    ).subscribe(data => {this.product = data,
    this.order.product=data}
    );
  
  }

  addProduct() {
   
    this.ps.add(this.product).subscribe(() => {
      this.router.navigate(['/']);
    })
  }

  toggleModal() {
    this.modalOpen = !this.modalOpen; 

  }


}
