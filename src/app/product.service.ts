import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Product[]>(environment.apiUrl+'/api/product');
  }

  getById(id:number) {
    return this.http.get<Product>(environment.apiUrl+'/api/product/'+id);
  }

  add(product:Product){
    return this.http.post<Product>(environment.apiUrl+'/api/product', product);
  }

  put(product:Product){
    return this.http.put<Product>(environment.apiUrl+'/api/product/'+product.id, product);
  }

  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/product/'+id);
  }

  search(name:string) {
    const params = new HttpParams()
    .set('name', name);
    return this.http.get<Product[]>(environment.apiUrl+'/api/product',{params})
  }

  getProductsByCategoryId(id: number) {
    return this.http.get<Product[]>(environment.apiUrl + '/api/product/category/'+id);
  }

}