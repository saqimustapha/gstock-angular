import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category, Product } from '../entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products:Product[] = [];
  single?: Product;
  displayEditForm: boolean = false;
  modifiedPr: Product = {name: "", price: 0, quantity:0};
  nameFilter?:string;
  
  
  

  constructor(private ps: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.ps.getAll().subscribe(data => this.products = data);
  }

  order(id:number) {
    this.router.navigateByUrl('/modal/'+id);
  }

  fetchOne() {
    this.ps.getById(2).subscribe(data => this.single = data);
  }

  fetchAll() {
    this.ps.getAll().subscribe(data => this.products = data);
  }

  delete(id:number) {
    this.ps.delete(id).subscribe();
    this.removeFromList(id);
  }
 
  removeFromList(id: number) {
    this.products.forEach((product, product_index) => {
      if(product.id == id) {
        this.products.splice(product_index, 1);
      }
    });
  }

  showEditForm(product:Product) {
    this.displayEditForm = true;
    this.modifiedPr = Object.assign({},product);
  }

  update(product:Product) {
    
    this.ps.put(product).subscribe();
    this.updateProductInList(product);
    this.displayEditForm = false;
  }

  updateProductInList(product:Product) {
    this.products.forEach((pr_list) => {
      if(pr_list.id == product.id) {
        pr_list.name = product.name;
        pr_list.price = product.price;
        pr_list.quantity = product.quantity;
      }
    });
  }

}
