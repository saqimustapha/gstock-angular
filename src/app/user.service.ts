import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from './entities';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(environment.apiUrl+'/api/admin/user');
  }

  changeRole(id: number, role: string) {
    return this.http.patch<void>(environment.apiUrl+'/api/admin/user/' + id + '/role/' + role, null);

  }
}
