import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { CategoryComponent } from './category/category.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ModalComponent } from './modal/modal.component';
import { ProductsByCategoryComponent } from './products-by-category/products-by-category.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { OrderingComponent } from './ordering/ordering.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { ChangePasswordComponent } from './change-password/change-password.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddProductComponent,
    SearchComponent,
    CategoryComponent,
    ModalComponent,
    ProductsByCategoryComponent,
    SingleProductComponent,
    OrderingComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    AllUsersComponent,
    ChangePasswordComponent,
   
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    
    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
