import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { AddProductComponent } from './add-product/add-product.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { ModalComponent } from './modal/modal.component';
import { ProductsByCategoryComponent } from './products-by-category/products-by-category.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { OrderingComponent } from './ordering/ordering.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthenticatedGuard } from './authenticated.guard';


const routes: Routes = [
  {path: '', component: HomeComponent, canActivate:[AuthenticatedGuard]},
  {path: 'add', component: AddProductComponent, canActivate:[AuthenticatedGuard]},
  {path: 'search', component: SearchComponent, canActivate:[AuthenticatedGuard]},
  {path: 'category', component: CategoryComponent, canActivate:[AuthenticatedGuard]},
  {path: 'modal/:id', component: ModalComponent, canActivate:[AuthenticatedGuard]},
  {path: 'category/:id', component: ProductsByCategoryComponent, canActivate:[AuthenticatedGuard]},
  {path: 'single', component: SingleProductComponent, canActivate:[AuthenticatedGuard]},
  {path: 'ordering', component: OrderingComponent, canActivate:[AuthenticatedGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'all', component: AllUsersComponent, canActivate:[AuthenticatedGuard]},
  {path: 'change-password', component: ChangePasswordComponent, canActivate:[AuthenticatedGuard] },
  


 
   
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
