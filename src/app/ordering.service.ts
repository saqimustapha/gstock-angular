import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Ordering } from './entities';

@Injectable({
  providedIn: 'root'
})
export class OrderingService {

  constructor(private http:HttpClient) { }

  add(ordering:Ordering){
    return this.http.post<Ordering>(environment.apiUrl+'/api/ordering', ordering);
  }
}
